# driverApp
The idea for the app is that you can use it to book a ride home after a night out by entering the location and the number of people you are out with. The app then will search for possible BOBs in the area who have the desired number of seats available in their car. The app will make sure that you get in touch with that person so that you and your friends are brought home safely. You can also use the app to choose a BOB in your group of friends and also enter or sign up for a ride as a BOB. This way you can earn an extra penny as a BOB, and you can also earn points that you can use to get discounts on soft drinks at the pub or club.

## Getting started
Start the app by using the link: 
https://i492418.hera.fhict.nl/

For a better experience create a shortcut on your mobile homescreen. This way you will have a better experience of the Drivr PWA. 

## Features
- Find rides from the location you are at
- Create rides as a BOB driver
- Game that chooses a BOB (sober driver)
- Send messages to driver
- Chat with drivers or other friends
- Rate drivers
- Gather points by driving as a BOB (and exchange the points for coupons and discounts)
- Connect your payment card and driver license
- View your travel history and transaction history. 
- Chat with the support team
## Installation
### Requirements
- Stylesheet Drivr
- Material Icons Google
### Install for development
After cloning the repository all you have to do is open the project in your code editor.
### Install on Mobile
Installing the PWA om your homescreen can be done with the following steps: 
- Go to the Drivr website (https://i492418.hera.fhict.nl/)
- Click on the 3 menu dots next to the address bar at the top of your screen 
- Click the "Add to homescreen" option 
After following these steps there will be a shortcut added to your homescreen.
### Install on Dekstop
Installing the PWA on the deckstop is as follows: 
 - Open Chrome on your computer (other browsers may not support PWA downloads) 
 - Go to the Drivr website (https://i492418.hera.fhict.nl/)
-  Click Install at the top of the adress bar
 - Install the PWA by following the on-screen instructions

## Usage
### Plan a ride
By clicking the button "plan ride" you can book a ride or create a ride depending what your purpose is. 
### Who's Bobbin'
By clicking on the "Who's Bobbin'" button you can find the game who chooses the BOB among your firend group you are going out with. 
### Sending a message
By clicking on the chat icon on the top of the homescreen you can communicate with the driver or with friends. 
### Side menu
By using the side menu you can navigate to different pages such as ride history, points, payment and support. 

## Support
If you have any questions, please feel free to contact us by emailing info.drivr@gmail.com.

## Authors and acknowledgment
Created by Munira Abdulahad, Ravi Bruggink, Gabriella Gebroers, Ciaran van Gool and Rens Leenen.

Thanks to Fontys University of Applied Sciences for the opportunity to work on this project and for the help they gave us. 
Thanks to Partout for presenting this assigment to us and assigining this project. 

## Project status
This project started on 2/9/2022







