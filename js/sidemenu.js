const burger =  document.getElementById("sidemenu-item");
const sideclick = document.getElementById("sidemenu-transparent-block");
const body = document.getElementsByTagName("BODY")[0];

burger.addEventListener("click", function(){
    document.getElementById("sidemenu").style.transform = "translateX(0%)";
    sideclick.style.width = "10%";
    body.style.overflow = "hidden";
});

sideclick.addEventListener("click", function(){
    document.getElementById("sidemenu").style.transform = "translateX(-100%)";
    sideclick.style.width = "0%";
    body.style.overflowY = "scroll";
});