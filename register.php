<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <title>Drivr - Register</title>
    <link rel="stylesheet" href="/css/stylesheet.css">
    <link rel="stylesheet" href="/css/register.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Exo:ital,wght@0,100;1,100&family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">

</head>
<body>
   
<?php
    require('db.php');
    // When form submitted, insert values into the database.
    if (isset($_REQUEST['phonenumber'])) {
        // removes backslashes
        $phonenumber = stripslashes($_REQUEST['phonenumber']);
        //escapes special characters in a string
        $phonenumber = mysqli_real_escape_string($con, $phonenumber);
        $email    = stripslashes($_REQUEST['email']);
        $email    = mysqli_real_escape_string($con, $email);
        $password = stripslashes($_REQUEST['password']);
        $password = mysqli_real_escape_string($con, $password);
        $create_datetime = date("Y-m-d H:i:s");
        $query    = "INSERT into `users` (phonenumber, password, email, create_datetime)
                     VALUES ('$phonenumber', '" . md5($password) . "', '$email', '$create_datetime')";
        $result   = mysqli_query($con, $query);
        if ($result) {
            echo "<div class='form'>
                  <h3>You are registered successfully.</h3><br/>
                  <p class='link'>Click here to <a href='login.php'>Login</a></p>
                  </div>";
        } else {
            echo "<div class='form'>
                  <h3>Required fields are missing.</h3><br/>
                  <p class='link'>Click here to <a href='registration.php'>registration</a> again.</p>
                  </div>";
        }
    } else {
?>
        <form class="form" action="" method="post">

            <h1 class="login-title">Sign up to Drivr</h1>

            <h2>Email</h2>
            <div class="input-box">
                <img src="./images/mail.svg" class="input-icons"></img>
                <input type="text" class="login-input" name="email" placeholder="Email Adress">
            </div>

            <h2>Phone Number</h2>
            <div class="input-box">
                <img src="./images/phone.svg" class="input-icons"></img>
                <input type="text" class="login-input" name="phonenumber" placeholder="Phone Number" required />
            </div>

            <h2>Password</h2>
            <div class="input-box">
                <img src="./images/lock.svg" class="input-icons"></img>
                <input type="password" class="login-input" name="password" placeholder="Password">
            </div>

            <div class="submit-box">
                <img src="./images/whitelock.svg" id="lock-icon">
                <input type="submit" name="submit" value="Create Account" class="login-button">
            </div>

            <div class="login-box">
                <p>Already a user?</p>
                <p><a href="login.php">Login</a></p>
            </div>
        </form> 
<?php
    }
?>
</body>
</html>